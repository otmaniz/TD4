#include <fcntl.h>
#include <sys/types.h>
//#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int lireligne(int fd, char *buffer, int size) {
	ssize_t nbread = read(fd, buffer, size);
	if (nbread == -1) {
		return -1;
	}

	int i;
	for (i = 0; i < nbread; i++) {
		if (buffer[i] == '\n') {
			i++;
			break;
		}
	}
	lseek(fd, i - nbread, SEEK_CUR);
	return i;
}


int main(int argc, char **argv) {

    // descripteur de fichier du fichier ouvert en lecture
    int fd_in = open(argv[1], O_RDONLY);
    //perror
    if(fd_in == -1){
        perror("Le fichier d'entrée n'a pas pu être lu");
        return -1;
    }


    // descripteur de fichier du fichier ouvert en écriture
    int fd_out = open(argv[2], O_WRONLY | O_CREAT,0666);
    //perror
    if(fd_out == -1){
        perror("Erreur lors de l'ouverture du fichier de sortie");
        return -1;
    }


    //lire l'en-tête
    char buffer[100];
    int nbread = lireligne(fd_in, buffer, sizeof(buffer));
    //perror
    if(nbread == -1){
        perror("Erreur de la lecture de l'en-tête");
        return -1;
    }


    //copie l'en-tete du nombre magique
    write(fd_out, buffer, nbread);

    // lire les variables (dimensions, niveau de gris)
    int largeur, longeur, niveauGris;
    lireligne(fd_in, buffer, sizeof(buffer));
    sscanf(buffer,"%d %d %d", &largeur, &longeur, &niveauGris);

    //écriture dans l'en-tête
    sprintf(buffer, "%d %d \n%d\n", largeur, longeur, niveauGris);
    write(fd_out, buffer, sizeof(buffer));

    //inversion negative pixel
    char pixel;
    while(read(fd_in, &pixel, sizeof(char)) != '\0'){
        char pixelInversé = (char)(niveauGris-pixel);
        write(fd_out, &pixelInversé, sizeof(char));
    }


    close(fd_in);
    close(fd_out);
    free(buffer);

    return 0;
}